#include <arduino2.h>
#include "MeOrion.h"
#include <AccelStepper.h>
//#include <PinChangeInt.h>
//#include <PinChangeIntConfig.h>

/*
To encode the running speed and trigger the camera

Digital input:
chA: Pin3, channel A of the rotary encoder
chB: Pin2, channel B of the rotary encoder  
lick: Pin14, licometer output

Digital output:
WheelDir: Pin4, the level denotes to the direction of wheel spining
WheelStp: Pin5, each pulse denotes to a step (on the direction according to Pin4) of rotation
CameraTrig: Pin7, upper edge to start recording, lower edge to stop

*/ 

#define stepPulseDur 100  // wheel step pulse duration in us
#define cycle 4000 // 4000 steps for each cycle of rotary encoder
#define start_record 2 // animal runs 2 cycles to start recording
#define stop_record 2 //the recording will stop after the animal runs another 3 cycles

#define driverPulseOcp 200 // ocupacy of the LED driver pulse (0 to 255)
#define stepsPerRevolution 200 // resolution of the stepper motor
#define half_p 5 // half period of stepper motor step, in ms
#define motorstep 50 //step of motor, angle = motorstep*1.8
#define optoProb 50 // probability of optogenetic trials in percentage
#define stimProb 100 // probability of stimulation trials in percentage



volatile long clicks; // Variable used to store wheel movements. Volatile is important
                      // for variables that can change in an interrupt
const int chA = 3;  // Pin 3, channel A
const int chB = 2;  // Pin 2, channel B
const int WheelDir = 4;  // Pin 4, wheel direction. 1 for clockwise, 0 for anti-clockwise
const int WheelStp = 5;  // Pin 5, a pulse at each click
//const int TurnCount = 6; // Pin6, a pulse at each turn
const int CameraTrig = 7;  //Pin 7, camera trigger, rising edge for trigger on, falling edge for trigger off
const int LickInput = 8;  // Pin8 (PB0), from the output of touch sensor
const int LEDDriver = 6; // Pin 6, the driver signal for LED
const int Reward = 11; // Pin 11, indicator & driver of reward presentation
const int Opto = 12; //Pin 12, indicator of if opto stim is given
const int Stim = 13; // Pin 13, indicator of if post whisker stim is given
const int stpPin = 9; //Pin9, pulse for the stepper motor
const int dirPin = 10; // Pin10, direction of he stepper motor


bool opto = false;
bool post = false;
bool camera_state = false;
bool last_camera_state = false;
int i = 0;
volatile int record_count = 0;
volatile unsigned long stim_start_time = 0;
volatile int turns;
volatile unsigned long trial_start_time = 0;


void setup() {
  // put your setup code here, to run once:
  pinMode(chB, INPUT);
  pinMode(chA, INPUT);
  pinMode(LickInput, INPUT_PULLUP);
  
  pinMode(WheelDir, OUTPUT);
  pinMode(WheelStp, OUTPUT);
  //pinMode(TurnCount, OUTPUT);
  pinMode(CameraTrig, OUTPUT);
  pinMode(LEDDriver, OUTPUT);
  pinMode(stpPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(Reward, OUTPUT);
  pinMode(Opto, OUTPUT);
  pinMode(Stim, OUTPUT);
  digitalWrite2(Stim, LOW);
  digitalWrite2(Opto, LOW);
  digitalWrite2(Reward, LOW);  
  randomSeed(analogRead(0)); // keep the analogue 0 unconnected, the analogue noise serve as the seed
  clicks = 0;
  turns = 0;
  Serial.begin(9600);
  //PCintPort::attachInterrupt(LickInput, lick, RISING);
  PCICR |= 0b00000001;    // turn on port b
  PCMSK0 |= 0b00000001;  //turn on 8
  
  attachInterrupt(digitalPinToInterrupt(chA), quadA, CHANGE); // CHANGE = whenever input goes HIGH or LOW on chA, call function quadA
  attachInterrupt(digitalPinToInterrupt(chB), quadB, CHANGE); // And for chB
}

void loop() {
  // put your main code here, to run repeatedly:

    camera_state = digitalRead(CameraTrig);

  // compare the buttonState to its previous state
  if (camera_state != last_camera_state) {
    // if the state has changed, increment the counter
    if (camera_state == HIGH) {
      driver();
    } else {
      opto = false;
      post = false;
      digitalWrite2(Stim, LOW);
      digitalWrite2(Opto, LOW);
    }
    // Delay a little bit to avoid bouncing
    delay(50);
  }
  last_camera_state = camera_state;
  //Serial.println(opto);
}

// for the 1000CPR encoder, combine chA and chB gets 4000CPR resolution
void quadA() {
  // If chA (int 0) state CHANGES, this interrupt is called.
  if ((PIND & (1 << PIND3)) == 0) { //if chA is HIGH
    if ((PIND & (1 << PIND2)) == 0) { //if chB is HIGH
      clicks++; // Wheel is turning backwards/clockwise (mouse running forwards)
      digitalWrite2(WheelDir, HIGH);
      if (clicks == cycle){
        clicks = 0;
        turns++;
        //digitalWrite2(TurnCount, HIGH);
        delayMicroseconds(stepPulseDur);
        //digitalWrite2(TurnCount, LOW); 
        if (digitalRead(CameraTrig)==0){ // if Camera trigger is LOW
           if ((turns >= start_record)&(turns < start_record+ stop_record)){
            digitalWrite2(CameraTrig, HIGH);
            
             trial_start_time = millis();
          }
        }
        else{         //Camera trigger is high
          if (turns == start_record + stop_record){
              if (millis() - trial_start_time < 1000){
              delay(1000 - (millis() - trial_start_time));
              }
            digitalWrite2(CameraTrig, LOW);
            turns = 0;
          }
        }
      }
    }
    else {
      clicks--; // Wheel is turning forwards / counter-clockwise (mouse "running" backwards)
      digitalWrite2(WheelDir, LOW);
      if (clicks == -1){
        clicks = cycle-1;
        turns--;
      }
    }
  }
  else { 
    if ((PIND & (1 << PIND2)) == 0) { //if chB is HIGH
      clicks--; // Wheel is turning fowards
      digitalWrite2(WheelDir, LOW);
      if (clicks == -1){
        clicks = cycle-1;
        turns--;
      }
    }
    else {
      clicks++; // Wheel is turning backwards
      digitalWrite2(WheelDir, HIGH);
      if (clicks == cycle){
        clicks = 0;
        turns++;
        //digitalWrite2(TurnCount, HIGH);
        delayMicroseconds(stepPulseDur);
        //digitalWrite2(TurnCount, LOW); 
        if (digitalRead(CameraTrig)==0){ // if Camera trigger is LOW
           if ((turns >= start_record)&(turns < start_record+ stop_record)){
            digitalWrite2(CameraTrig, HIGH);
            trial_start_time = millis();
          }
        }
        else{ //Camera trigger is high
          if (turns == start_record + stop_record){
              if (millis() - trial_start_time < 1000){
               delay(1000 - (millis() - trial_start_time));
               }
            digitalWrite2(CameraTrig, LOW);
            turns = 0;
          }
        }
      }
    }
  }
  digitalWrite2(WheelStp, HIGH);
  delayMicroseconds(stepPulseDur);
  digitalWrite2(WheelStp, LOW);  
}

void quadB() {
  // If chB state CHANGES, this interrupt is called.
  if ((PIND & (1 << PIND2)) == 0) { //if chB is HIGH
    if ((PIND & (1 << PIND3)) == 0) { //if chA is HIGH
      clicks--; // Wheel is turning forwards / counter-clockwise (mouse "running" backwards):
      digitalWrite2(WheelDir, LOW);
      if (clicks == -1){
        clicks = cycle-1;
        turns--;
      }
    }
    else {
      clicks++; // Wheel is turning backwards/clockwise (mouse running forwards)
      digitalWrite2(WheelDir, HIGH);
      if (clicks == cycle){
        clicks = 0;
        turns++;
        //digitalWrite2(TurnCount, HIGH);
        delayMicroseconds(stepPulseDur);
        //digitalWrite2(TurnCount, LOW); 
        if (digitalRead(CameraTrig)==0){ // if Camera trigger is LOW
           if ((turns >= start_record)&(turns < start_record+ stop_record)){
            digitalWrite2(CameraTrig, HIGH);
            trial_start_time = millis();
          }
        }
        else{ //Camera trigger is high
          if (turns == start_record + stop_record){
               if (millis() - trial_start_time < 1000){
               delay(1000 - (millis() - trial_start_time));
               }
            digitalWrite2(CameraTrig, LOW);
            turns = 0;
          }
        }
      }
    }
  }
  else { 
    if ((PIND & (1 << PIND3)) == 0) { //if chA is HIGH
      clicks++; // Wheel is turning backwards
      digitalWrite2(WheelDir, HIGH);
      if (clicks == cycle){
        clicks = 0;
        turns++;
        //digitalWrite2(TurnCount, HIGH);
        delayMicroseconds(stepPulseDur);
        //digitalWrite2(TurnCount, LOW); 
        if (digitalRead(CameraTrig)==0){ // if Camera trigger is LOW
           if ((turns >= start_record)&(turns < start_record+ stop_record)){
            digitalWrite2(CameraTrig, HIGH);
            trial_start_time = millis();
          }
        }
        else{ //Camera trigger is high
          if (turns == start_record + stop_record){
            if (millis() - trial_start_time < 1000){
              delay(1000 - (millis() - trial_start_time));
              }
            digitalWrite2(CameraTrig, LOW);
            turns = 0;
          }
        }
      }
    }
    else {
      clicks--; // Wheel is turning forwards
      digitalWrite2(WheelDir, LOW);
      if (clicks == -1){
        clicks = cycle-1;
        turns--;
      }
    }
  }
  digitalWrite2(WheelStp, HIGH);
  delayMicroseconds(stepPulseDur);
  digitalWrite2(WheelStp, LOW); 
}

void driver() {
  record_count++;
  if (random(100)<=optoProb){
    opto = true;
    digitalWrite2(Opto, HIGH);
  }
  else {opto = false;}
  
  if (random(100)<=stimProb){
    post = true;
    digitalWrite2(Stim, HIGH);
  }
  else {post = false;}
  
  if (post && opto){
    //stepper.moveTo(20);
    analogWrite(LEDDriver,driverPulseOcp);
    digitalWrite2(dirPin, HIGH);
    i = 0;
    while (i<=motorstep){
      digitalWrite2(stpPin, HIGH);
      delay(half_p);
      digitalWrite2(stpPin, LOW);
      delay(half_p);
      i=i+1;
      }
    stim_start_time = millis();
    delay(9000); 
    analogWrite(LEDDriver,0);
    digitalWrite2(dirPin, LOW);
    i = 0;
    while (i<=motorstep){
      digitalWrite2(stpPin, HIGH);
      delay(half_p);
      digitalWrite2(stpPin, LOW);
      delay(half_p);
      i=i+1;
      }
  }
  else if (!post && opto){
    analogWrite(LEDDriver,driverPulseOcp);
    delay(500);
    stim_start_time = millis();
    delay(9500); 
    analogWrite(LEDDriver,0);
  }
  else if (post && !opto){
    digitalWrite2(dirPin, HIGH);
    i = 0;
    while (i<=motorstep){
      digitalWrite2(stpPin, HIGH);
      delay(half_p);
      digitalWrite2(stpPin, LOW);
      delay(half_p);
      i=i+1;
      }
    stim_start_time = millis();
    delay(9000); 
    digitalWrite2(dirPin, LOW);
    i = 0;
    while (i<=motorstep){
      digitalWrite2(stpPin, HIGH);
      delay(half_p);
      digitalWrite2(stpPin, LOW);
      delay(half_p);
      i=i+1;
      }
  }
  else {
  delay(500);
  stim_start_time = millis();
  delay(9500);
  }
  opto = false;
  post = false;
  digitalWrite2(Stim, LOW);
  digitalWrite2(Opto, LOW);
}

void lick(){
  if ((millis()-stim_start_time <= 5000)&&(stim_start_time != 0)){
    if (digitalRead(Stim) == HIGH){
        digitalWrite2(Reward, HIGH);
        delay(5000); //pulse width of reward signal
        digitalWrite2(Reward, LOW);
      }
    else{
      delay(50000); //time out
      }
  }
  }

  
ISR(PCINT0_vect){    // Port C, PCINT8 - PCINT14
  if (PINB & bit (0)){
    lick();
    }
  } 
