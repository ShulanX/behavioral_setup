#include "MeOrion.h"
#include <AccelStepper.h>
#include <arduino2.h>
 #define driverPulseOcp 200 // ocupacy of the LED driver pulse (0 to 255)
 #define stepsPerRevolution 200 // resolution of the stepper motor
 #define half_p 5 // half period of stepper motor step, in ms
 #define motorstep 50 //step of motor, angle = motorstep*1.8
 #define optoProb 50 // probability of optogenetic trials in percentage
 #define stimProb 100 // probability of stimulation trials in percentage

const int CameraTrig = 2; //Pin2, input from Pin 5 from the other arduino 
const int LickInput = 3;  // Pin3, from the output of touch sensor
const int LEDDriver = 9; // Pin 9, the driver signal for LED
const int Reward = 11; // Pin 11, indicator & driver of reward presentation
const int Opto = 12; //Pin 12, indicator of if opto stim is given
const int Stim = 13; // Pin 13, indicator of if post whisker stim is given
const int stpPin = 4; //Pin4, pulse for the
const int dirPin = 5;
//AccelStepper stepper(AccelStepper::DRIVER, stpPin, dirPin);

bool opto = false;
bool post = false;
bool camera_state = false;
bool last_camera_state = false;
int i = 0;
volatile int record_count = 0;
volatile unsigned long stim_start_time = 0;
volatile unsigned long trial_start_time = 0;


void setup() {
  // put your setup code here, to run once:
  pinMode(CameraTrig, INPUT);
  pinMode(LickInput, INPUT);
  pinMode(LEDDriver, OUTPUT);
  pinMode(stpPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(Reward, OUTPUT);
  pinMode(Opto, OUTPUT);
  pinMode(Stim, OUTPUT);
  randomSeed(analogRead(0)); // keep the analogue 0 unconnected, the analogue noise serve as the seed
  digitalWrite2(Stim, LOW);
  digitalWrite2(Opto, LOW);
  digitalWrite2(Reward, LOW); 
  attachInterrupt(digitalPinToInterrupt(LickInput), lick, RISING);
  Serial.begin(9600);
//  stepper.setMaxSpeed(100);
//  stepper.setAcceleration(200);
}

void loop() {
  // put your main code here, to run repeatedly: 
    camera_state = digitalRead(CameraTrig);
    //stepper.run();
    //Serial.println(digitalRead(Stim));

  // compare the buttonState to its previous state
  if (camera_state != last_camera_state) {
    // if the state has changed, increment the counter
    if (camera_state == HIGH) {
      trial_start_time = millis();
      driver();
    } else {
      opto = false;
      post = false;
      digitalWrite2(Stim, LOW);
      digitalWrite2(Opto, LOW);
    }
    // Delay a little bit to avoid bouncing
    delay(50);
  }
  last_camera_state = camera_state;
  Serial.println(opto);
}
/*
bool is_running (){
  last_click = clicks;
  delay(100); //wait for 100ms to see if the animal run for more than 100 clicks
  current_click = clicks;
  if (current_click >= last_run_state + 100){
    return true;
  }
  else {
    return false;
  }
}
*/
void driver() {
  record_count++;
  if (random(100)<=optoProb){
    opto = true;
    digitalWrite2(Opto, HIGH);
  }
  else {opto = false;}
  
  if (random(100)<=stimProb){
    post = true;
    digitalWrite2(Stim, HIGH);
  }
  else {post = false;}
  
  if (post && opto){
    //stepper.moveTo(20);
    analogWrite(LEDDriver,driverPulseOcp);
    digitalWrite2(dirPin, HIGH);
    i = 0;
    while (i<=motorstep){
      digitalWrite2(stpPin, HIGH);
      delay(half_p);
      digitalWrite2(stpPin, LOW);
      delay(half_p);
      i=i+1;
      }
    stim_start_time = millis();
    delay(9000); 
    analogWrite(LEDDriver,0);
    digitalWrite2(dirPin, LOW);
    i = 0;
    while (i<=motorstep){
      digitalWrite2(stpPin, HIGH);
      delay(half_p);
      digitalWrite2(stpPin, LOW);
      delay(half_p);
      i=i+1;
      }
  }
  else if (!post && opto){
    analogWrite(LEDDriver,driverPulseOcp);
    delay(500);
    stim_start_time = millis();
    delay(9500); 
    analogWrite(LEDDriver,0);
  }
  else if (post && !opto){
    digitalWrite2(dirPin, HIGH);
    i = 0;
    while (i<=motorstep){
      digitalWrite2(stpPin, HIGH);
      delay(half_p);
      digitalWrite2(stpPin, LOW);
      delay(half_p);
      i=i+1;
      }
    stim_start_time = millis();
    delay(9000); 
    digitalWrite2(dirPin, LOW);
    i = 0;
    while (i<=motorstep){
      digitalWrite2(stpPin, HIGH);
      delay(half_p);
      digitalWrite2(stpPin, LOW);
      delay(half_p);
      i=i+1;
      }
  }
  else {
  delay(500);
  stim_start_time = millis();
  delay(9000);
  }
  opto = false;
  post = false;
  digitalWrite2(Stim, LOW);
  digitalWrite2(Opto, LOW);
}

void lick(){
  if ((millis()-stim_start_time <= 5000)&&(stim_start_time != 0)){
    if (digitalRead(Stim) == HIGH){
        digitalWrite2(Reward, HIGH);
        delay(1000); //pulse width of reward signal
        digitalWrite2(Reward, LOW);
      }
    else{
      delay(50000); //time out
      }
  }
  }
