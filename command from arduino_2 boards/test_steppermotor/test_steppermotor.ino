#include <arduino2.h>
 #define driverPulseOcp 200 // ocupacy of the LED driver pulse (0 to 255)
 #define stepsPerRevolution 200 // resolution of the stepper motor
 #define sps 1 // speed of stepper motor, step per second
 #define motorstep 50 //step of motor, angle = motorstep*1.8

const int stpPin = 4; //Pin4, pulse for the
const int dirPin = 5;
int i = 0;
int half_p = 1/sps/2*1000; //half delay time in ms

void setup() {
  // put your setup code here, to run once:
  pinMode(stpPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
}

void loop() {
  
  // put your main code here, to run repeatedly:
    digitalWrite2(dirPin, LOW);
    i = 0;
    while (i<=motorstep){
      digitalWrite2(stpPin, HIGH);
      delay(10);
      digitalWrite2(stpPin, LOW);
      delay(10);
      i=i+1;
      }
      delay(100);

}
