#include <arduino2.h>

/*
To encode the running speed and trigger the camera

Digital input:
chA: Pin3, channel A of the rotary encoder
chB: Pin2, channel B of the rotary encoder  

Digital output:
WheelDir: Pin4, the level denotes to the direction of wheel spining
WheelStp: Pin5, each pulse denotes to a step (on the direction according to Pin4) of rotation
TurnCount: Pin6, each pulse denotes to a turn
CameraTrig: Pin7, upper edge to start recording, lower edge to stop


*/ 
 
#define stepPulseDur 100  // wheel step pulse duration in us
#define cycle 4000 // 4000 steps for each cycle of rotary encoder
#define start_record 2 // animal runs 2 cycles to start recording
#define stop_record 2 //the recording will stop after the animal runs another 3 cycles

volatile long clicks; // Variable used to store wheel movements. Volatile is important
                      // for variables that can change in an interrupt
const int chA = 3;  // Pin 3, channel A
const int chB = 2;  // Pin 2, channel B
const int WheelDir = 4;  // Pin 4, wheel direction. 1 for clockwise, 0 for anti-clockwise
const int WheelStp = 5;  // Pin 5, a pulse at each click
const int TurnCount = 6; // Pin6, a pulse at each turn
const int CameraTrig = 7;  //Pin 7, camera trigger, rising edge for trigger on, falling edge for trigger off
volatile int turns;
volatile long trial_start_time = 0;


void setup() {
  // put your setup code here, to run once:
  pinMode(chB, INPUT);
  pinMode(chA, INPUT);
  pinMode(WheelDir, OUTPUT);
  pinMode(WheelStp, OUTPUT);
  pinMode(TurnCount, OUTPUT);
  pinMode(CameraTrig, OUTPUT);
  clicks = 0;
  turns = 0;
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(chA), quadA, CHANGE); // CHANGE = whenever input goes HIGH or LOW on chA, call function quadA
  attachInterrupt(digitalPinToInterrupt(chB), quadB, CHANGE); // And for chB
}

void loop() {
  // put your main code here, to run repeatedly:
   //Serial.println(digitalRead(CameraTrig));
}

// for the 1000CPR encoder, combine chA and chB gets 4000CPR resolution
void quadA() {
  // If chA (int 0) state CHANGES, this interrupt is called.
  if ((PIND & (1 << PIND3)) == 0) { //if chA is HIGH
    if ((PIND & (1 << PIND2)) == 0) { //if chB is HIGH
      clicks++; // Wheel is turning backwards/clockwise (mouse running forwards)
      digitalWrite2(WheelDir, HIGH);
      if (clicks == cycle){
        clicks = 0;
        turns++;
        digitalWrite2(TurnCount, HIGH);
        delayMicroseconds(stepPulseDur);
        digitalWrite2(TurnCount, LOW); 
        if (digitalRead(CameraTrig)==0){ // if Camera trigger is LOW
           if ((turns >= start_record)&(turns < start_record+ stop_record)){
            digitalWrite2(CameraTrig, HIGH);
             trial_start_time = millis();
          }
        }
        else{         //Camera trigger is high
          if (turns == start_record + stop_record){
              if (millis() - trial_start_time < 1000){
              delay(1000 - (millis() - trial_start_time));
              }
            digitalWrite2(CameraTrig, LOW);
            turns = 0;
          }
        }
      }
    }
    else {
      clicks--; // Wheel is turning forwards / counter-clockwise (mouse "running" backwards)
      digitalWrite2(WheelDir, LOW);
      if (clicks == -1){
        clicks = cycle-1;
        turns--;
      }
    }
  }
  else { 
    if ((PIND & (1 << PIND2)) == 0) { //if chB is HIGH
      clicks--; // Wheel is turning fowards
      digitalWrite2(WheelDir, LOW);
      if (clicks == -1){
        clicks = cycle-1;
        turns--;
      }
    }
    else {
      clicks++; // Wheel is turning backwards
      digitalWrite2(WheelDir, HIGH);
      if (clicks == cycle){
        clicks = 0;
        turns++;
        digitalWrite2(TurnCount, HIGH);
        delayMicroseconds(stepPulseDur);
        digitalWrite2(TurnCount, LOW); 
        if (digitalRead(CameraTrig)==0){ // if Camera trigger is LOW
           if ((turns >= start_record)&(turns < start_record+ stop_record)){
            digitalWrite2(CameraTrig, HIGH);
            trial_start_time = millis();
          }
        }
        else{ //Camera trigger is high
          if (turns == start_record + stop_record){
              if (millis() - trial_start_time < 1000){
               delay(1000 - (millis() - trial_start_time));
               }
            digitalWrite2(CameraTrig, LOW);
            turns = 0;
          }
        }
      }
    }
  }
  digitalWrite2(WheelStp, HIGH);
  delayMicroseconds(stepPulseDur);
  digitalWrite2(WheelStp, LOW);  
}

void quadB() {
  // If chB state CHANGES, this interrupt is called.
  if ((PIND & (1 << PIND2)) == 0) { //if chB is HIGH
    if ((PIND & (1 << PIND3)) == 0) { //if chA is HIGH
      clicks--; // Wheel is turning forwards / counter-clockwise (mouse "running" backwards):
      digitalWrite2(WheelDir, LOW);
      if (clicks == -1){
        clicks = cycle-1;
        turns--;
      }
    }
    else {
      clicks++; // Wheel is turning backwards/clockwise (mouse running forwards)
      digitalWrite2(WheelDir, HIGH);
      if (clicks == cycle){
        clicks = 0;
        turns++;
        digitalWrite2(TurnCount, HIGH);
        delayMicroseconds(stepPulseDur);
        digitalWrite2(TurnCount, LOW); 
        if (digitalRead(CameraTrig)==0){ // if Camera trigger is LOW
           if ((turns >= start_record)&(turns < start_record+ stop_record)){
            digitalWrite2(CameraTrig, HIGH);
            trial_start_time = millis();
          }
        }
        else{ //Camera trigger is high
          if (turns == start_record + stop_record){
               if (millis() - trial_start_time < 1000){
               delay(1000 - (millis() - trial_start_time));
               }
            digitalWrite2(CameraTrig, LOW);
            turns = 0;
          }
        }
      }
    }
  }
  else { 
    if ((PIND & (1 << PIND3)) == 0) { //if chA is HIGH
      clicks++; // Wheel is turning backwards
      digitalWrite2(WheelDir, HIGH);
      if (clicks == cycle){
        clicks = 0;
        turns++;
        digitalWrite2(TurnCount, HIGH);
        delayMicroseconds(stepPulseDur);
        digitalWrite2(TurnCount, LOW); 
        if (digitalRead(CameraTrig)==0){ // if Camera trigger is LOW
           if ((turns >= start_record)&(turns < start_record+ stop_record)){
            digitalWrite2(CameraTrig, HIGH);
            trial_start_time = millis();
          }
        }
        else{ //Camera trigger is high
          if (turns == start_record + stop_record){
            if (millis() - trial_start_time < 1000){
              delay(1000 - (millis() - trial_start_time));
              }
            digitalWrite2(CameraTrig, LOW);
            turns = 0;
          }
        }
      }
    }
    else {
      clicks--; // Wheel is turning forwards
      digitalWrite2(WheelDir, LOW);
      if (clicks == -1){
        clicks = cycle-1;
        turns--;
      }
    }
  }
  digitalWrite2(WheelStp, HIGH);
  delayMicroseconds(stepPulseDur);
  digitalWrite2(WheelStp, LOW); 
}

/*
void camera_trigger(){
  if ((PIND & (1 << PIND7)) == 0){ // if Camera trigger is HIGH
    if (turns == start_record + stop_record){
      digitalWrite2(CameraTrig, LOW);
      turns = 0;
      }
    }
  else{ //Camera trigger is low
    if ((turns >= start_record)&(turns < start_record+ stop_record)){
      digitalWrite2(CameraTrig, HIGH);
    }
  }
}
*/
