# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29 16:53:12 2019

@author: dalan

gui for contolling the setup

hardware connection:
    
input:
port 0 bit 0: Wheelstp
port 0 bit 1: Wheeldir
port 0 bit 2: opto
port 0 bit 3: stim
port 0 bit 4: trigger
port 0 bit 5: licometer
port 0 bit 6: reward

output:
port 1 bit 0 (bit 8): optogenetic driver
port 1 bit 1-2 (bit 9-10): stepper motor driver dir & pul
port 1 bit 3 (bit 11): opto enable signal 
port 1 bit 4 (bit 12): stim enable signal
port 1 bit 5 (bit 13): trigger
port 1 bit 6 (bit 14): water reward
"""
from digital_device_management_function import load_all_digital_ports
from digital_device_management_function import set_digital_in_channels, set_digital_out_channels
from digital_device_management_function import digital_bit_in, digital_bit_out, digital_port_in

from scan_function import *

from LedIndicatorWidget import *

import queue
import threading
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import numpy as np

from matplotlib.backends.qt_compat import QtCore, QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvas

import sys
from PyQt5 import QtCore
# from pyqtgraph.Qt import QtCore
from PyQt5.QtWidgets import *
from PyQt5 import QtWidgets, uic
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg


# def board_setup(board_num = 0, verbool = False):
#     """
#     initialize the in/out port, test the readout
#     the first port (FirstPortA) is set as input and the second (FirstPortB) as output

#     Parameters
#     ----------
#     board_num : TYPE, optional
#         DESCRIPTION. The default is 0.
#     verbool : bool, optional
#         DESCRIPTION. The default is False.

#     Returns
#     -------
#     ports: all the ports on the board

#     """
#     try: 
#         ports = load_all_digital_ports(board_num)
#         set_digital_in_channels(ports[0], board_num)
#         set_digital_out_channels(ports[1], board_num)
#         in_port_value = digital_port_in(ports[0], board_num)
#         out_port_value = digital_port_in(ports[1], board_num)
#         if verbool:
#             print(ports[0].type.name + ' is set as an input port. The value is ' + str(in_port_value))
#             print(ports[1].type.name + ' is set as a output port. The last output value is ' + str(out_port_value))
#         return ports
    
#     except:
#         print('can\'t load the ports, try to run InstaCal to load the board')
#         return

# def set_command_source(self, command_source):
#     """
    
#     Parameters
#     ----------
#     command_source : bool
#         1 for sending driver signal from python, 0 for sending from arduino
        
#     Returns
#     -------
#     None.

#     """
#         self.stim_param.update({'command_source' : command_source})
        
# def set_opto_stim(stim_param, if_activate, intensity, p = 0.5, freq = 980, dur = 1):
#     stim_param.update({
#         'opto': if_activate,
#         'intensity': intensity,
#         'opto_p': p,
#         'freq': freq,
#         'dur': dur
#         })
    
# def set_post_stim(stim_param, if_activate, p = 0.5, step_num = 20, sps = 100):
#     stim_param.update({
#         'stim': if_activate,
#         'stim_p': p,
#         'step_num': step_num,
#         'sps': sps
#         })
    
# def set_trigger(stim_param, if_activate, trigger_type = 1, turns_before = 2, turns_during = 2):
#     stim_param.update({
#         'trigger': if_activate,
#         'trigger_type': trigger_type,
#         'turns_before': turns_before,
#         'turns_during': turns_during
#         })
    
# def start_process(fs = 100e3, stim_param = None):
#     q = queue.Queue(100)
#     ports = board_setup(board_num = 0)
#     process = ScanTreading(q, port = ports[0], fs = fs, board_num = 0)
#     process.start()
#     # print('started')
#     plot_thread = threading.Thread(target = speed_plot, daemon = True, args = (process, ports, stim_param, ))
#     plot_thread.start()
#     return process#, plot_thread
    
  
# def stop_process(process):
#     process.stop()


# def drivers(accum_stp, last_stp, trigger_command_state, ports, command_source,
#             opto, intensity, opto_p, freq, dur, stim, stim_p, step_num, sps,
#             trigger, trigger_type, turns_before, turns_during):
#     """
#     drivers based on kwaargs, called in the while loop inside speed_plot stimulus part

#     Parameters
#     ----------
#     **kwargs : dict
#         opto (bool)
#         stim (bool)
#         trigger (bool)
        
#         opto intensity
#         (opto probability)
#         (opto freq)
#         (opto dur)
        
#         (stim probability)
#         (stim step_num)
#         (stim sps)
        
#         trigger_type (1 for level, 0 for edge)
#         (turns_before)
#         (turns_during)

#     Returns
#     -------
#     None.

#     """
#     # print('accum_step: ' + str(accum_stp))
#     if trigger_command_state:
#         if accum_stp == last_stp + 20: #animal runs for half turn before stimulation 
#             if opto:
#                 if np.random.uniform()<=opto_p:
#                     opto_driver(intensity, port = ports[0],freq = freq, dur = dur)
#             if stim:
#                 if np.random.uniform()<=stim_p:
#                     stepper_driver(port = ports[0], step_num = step_num, sps = sps, dur = dur)
#         if accum_stp == last_stp + 40 *turns_during:
#             last_stp = accum_stp
#             trigger_command_state = 0
#             if trigger_type:
#                 trigger_level(0, port = ports[0])
#             else:
#                 trigger_pulse(port = ports[0])
#     else:
#         if accum_stp == last_stp + 40*turns_before:
#             last_stp = accum_stp
#             trigger_command_state = 1
#             if trigger_type:
#                 trigger_level(1, port = ports[0])
#             else:
#                 trigger_pulse(port = ports[0])
#     return last_stp, trigger_command_state
            
                
        
            
# def speed_plot(process, ports, stim_param):
#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1)
#     trigger_command_state = 0
#     last_stp = 0

#     i = 0
#     start_time = time.time()
#     x = []
#     y = []
#     speeds = []
    
#     while process.running:
#         # readout
#         value = process.q.get()
#         speed = value[2]
#         accum_stp = value[3]
#         opto_state = value[4]
#         stim_state = value[5]
#         trigger_state = value[6]
#         # print('current time: ' + str(time.time() - start_time))
        
#         # set stimulus
#         if stim_param['command_source']:
#             last_stp, trigger_command_state = drivers(accum_stp, last_stp, 
#                                                       trigger_command_state, ports, **stim_param)
        
        
#         # plot speed
#         speeds.append(speed)
#         if i%10 == 0:
#             # Add x and y to lists
#             x.append(time.time() - start_time)
#             y.append(np.mean(speeds))
#             speeds = []
            
#             # Limit x and y lists to 20 items
#             x = x[-20:]
#             y = y[-20:]
#             speed_plot_fun(x, y, ax)

#         # time.sleep(1/process.fs)
#         i = i + 1
        
    

# def speed_plot_fun(x, y, ax):
#     print('current time: ' + str(x[-1]) + ' current speed: ' + str(y[-1]))

#     # Draw x and y lists
#     ax.clear()
#     ax.plot(x, y)

#     # Format plot
#     plt.xticks(rotation=45, ha='right')
#     plt.subplots_adjust(bottom=0.30)
#     plt.title('speed over time')
#     plt.ylabel('click per ms')
      

class Behavioral_GUI(QDialog):
    def __init__(self, parent=None):
        super(Behavioral_GUI, self).__init__(parent)

        self.ports = None
        
        self.originalPalette = QApplication.palette()
        self.stim_param = {
            'command_source': 1,
            'opto': 1, 
            'intensity': 100, 
            'opto_p': 0.5, 
            'freq': 980, 
            'dur': 20, 
            'stim': 1, 
            'stim_p': 0.5, 
            'step_num': 100, 
            'sps': 10,
            'trigger': 1, 
            'trigger_type': 1, 
            'turns_before': 2, 
            'turns_during': 2,
            'reward': 1,
            'reward_pulse_width': 1,
            'time_out_dur': 10,
            'respond_window': 5
            }
        self.x = []
        self.y = []
        self.trigger_command_state = 0
        self.last_stp = 0
        self.reward_command_state = 0
        self.opto_state = 0
        self.stim_state = 0
        self.reward_state = 0
        self.trigger_state = 0
        self.lick_state = 0
        self.reward_state = 0
        self.respond = 0
        self.stim_flag = False
        # default values

        command_source_ComboBox = QComboBox()
        command_source_ComboBox.addItems(['from python', 'from arduino'])

        command_source_Label = QLabel("&Command source:")
        command_source_Label.setBuddy(command_source_ComboBox)
        
        command_source_ComboBox.activated[str].connect(self.set_command_source)

        self.opto_CheckBox = QCheckBox("Opto")
        self.opto_CheckBox.setChecked(True)
        self.opto_CheckBox.toggled.connect(self.activate_opto)
        
        self.stim_CheckBox = QCheckBox("Whisker stim")
        self.stim_CheckBox.setChecked(True)
        self.stim_CheckBox.toggled.connect(self.activate_stim)
        
        self.trigger_CheckBox = QCheckBox("Trigger")
        self.trigger_CheckBox.setChecked(True)
        self.trigger_CheckBox.toggled.connect(self.activate_trigger)
        
        self.reward_CheckBox = QCheckBox("Reward")
        self.reward_CheckBox.setChecked(True)
        self.reward_CheckBox.toggled.connect(self.activate_reward)

        self.createTopLeftGroupBox()
        self.createTopRightGroupBox()
        self.createBottomLeftGroupBox()
        self.createBottomRightGroupBox()
        self.createBottomPanel()
        self.createBottomLeftGroupBox2()
        self.createBottomRightGroupBox2()
        self.createBottomPanel2()
        # self.createBottomLeftTabWidget()
        # self.createBottomRightGroupBox()
        # self.createProgressBar()

        topLayout = QHBoxLayout()
        topLayout.addWidget(command_source_Label)
        topLayout.addWidget(command_source_ComboBox)
        topLayout.addStretch(1)
        topLayout.addWidget(self.opto_CheckBox)
        topLayout.addWidget(self.stim_CheckBox)
        topLayout.addWidget(self.trigger_CheckBox)
        topLayout.addWidget(self.reward_CheckBox)

        mainLayout = QGridLayout()
        mainLayout.addLayout(topLayout, 0, 0, 1, 2)
        mainLayout.addWidget(self.topLeftGroupBox, 1, 0)
        mainLayout.addWidget(self.topRightGroupBox, 1, 1)
        mainLayout.addWidget(self.bottomLeftGroupBox, 2, 0)
        mainLayout.addWidget(self.bottomRightGroupBox, 2, 1)
        mainLayout.addWidget(self.bottomLeftGroupBox2, 3, 0)
        mainLayout.addWidget(self.bottomRightGroupBox2, 3, 1)
        mainLayout.addWidget(self.speedplot, 4, 0, 1, 2)
        mainLayout.addWidget(self.bottomGroupBox2, 5, 0, 1, 2)
        mainLayout.setColumnStretch(0, 1)
        mainLayout.setColumnStretch(1, 1)
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setRowStretch(3, 1)
        mainLayout.setRowStretch(4, 4)
        mainLayout.setRowStretch(5, 1)
        self.setLayout(mainLayout)

        self.setWindowTitle("GUI")
        self.changeStyle('Windows')
    
            
    def set_command_source(self, command_source_text):
        """
        
        Parameters
        ----------
        command_source : bool
            1 for sending driver signal from python, 0 for sending from arduino
            
        Returns
        -------
        None.
    
        """
        if command_source_text == 'from python':
            command_source = 1
        else:
            command_source = 0
        self.stim_param.update({'command_source' : command_source})
    
    
    def activate_opto(self, if_activate):
        self.stim_param.update({'opto' : if_activate})
      
        
    def activate_stim(self, if_activate):
        self.stim_param.update({'stim' : if_activate})
      
        
    def activate_trigger(self, if_activate):
        self.stim_param.update({'trigger' : if_activate})
        
    def activate_reward(self, if_activate):
        self.stim_param.update({'reward' : if_activate})
        
    def set_intensity(self, value):
        self.stim_param.update({'intensity' : int(value)})
    
    def set_opto_p(self, default):
        if default:
            self.stim_param.update({'opto_p' : float(self.opto_p_text.text())})
        else:
            self.stim_param.update({'opto_p' : 0.5})
        
    def set_dur(self, default):
        if default:
            self.stim_param.update({'dur' : float(self.dur_text.text())})
        else:
            self.stim_param.update({'dur' : 20})        
        
    def set_freq(self, default):
        if default:
            self.stim_param.update({'freq' : int(self.freq_text.text())})
        else:
            self.stim_param.update({'freq' : 980})
            
    def set_stim_p(self, default):
        if default:
            self.stim_param.update({'stim_p' : float(self.stim_p_text.text())})
        else:
            self.stim_param.update({'stim_p' : 0.5})
        
    def set_step_num(self, default):
        if default:
            self.stim_param.update({'step_num' : float(self.step_num_text.text())})
        else:
            self.stim_param.update({'step_num' : 100})
        
        
    def set_sps(self, default):
        if default:
            self.stim_param.update({'sps' : int(self.sps_text.text())})
        else:
            self.stim_param.update({'sps' : 10})
            
    def set_trigger_type(self, default):
        if default:
            self.stim_param.update({'trigger_type' : int(self.trigger_type_text.text())})
        else:
            self.stim_param.update({'trigger_type' : 1})
        
    def set_turns_before(self, default):
        if default:
            self.stim_param.update({'turns_before' : float(self.turns_before_text.text())})
        else:
            self.stim_param.update({'turns_before' : 2})
        
        
    def set_turns_during(self, default):
        if default:
            self.stim_param.update({'turns_during' : int(self.turns_during_text.text())})
        else:
            self.stim_param.update({'turns_during' : 2})
            
    def set_reward_pulse_width(self, default):
        if default:
            self.stim_param.update({'reward_pulse_width' : self.reward_pulse_width_text.text()})
        else:
            self.stim_param.update({'reward_pulse_width' : 1})
    

    def board_setup(self, board_num = 0, verbool = False):
        """
        initialize the in/out port, test the readout
        the first port (FirstPortA) is set as input and the second (FirstPortB) as output
    
        Parameters
        ----------
        board_num : TYPE, optional
            DESCRIPTION. The default is 0.
        verbool : bool, optional
            DESCRIPTION. The default is False.
    
        Returns
        -------
        ports: all the ports on the board
    
        """
        try: 
            self.ports = load_all_digital_ports(board_num)
            ports = self.ports
            set_digital_in_channels(ports[0], board_num)
            set_digital_out_channels(ports[1], board_num)
            in_port_value = digital_port_in(ports[0], board_num)
            out_port_value = digital_port_in(ports[1], board_num)
            if verbool:
                print(ports[0].type.name + ' is set as an input port. The value is ' + str(in_port_value))
                print(ports[1].type.name + ' is set as a output port. The last output value is ' + str(out_port_value))
        
        except:
            print('can\'t load the ports, try to run InstaCal to load the board')
            
    
    def start_process(self, fs = 100e3):
        q = queue.Queue(100)
        if self.ports is None:
            self.ports = self.board_setup(board_num = 0)
        self.process = ScanTreading(q, port = self.ports[0], fs = 100e3, board_num = 0)
        self._interval = 1
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.load_values)

        self.start_time = time.time()
        self.stim_start_time = self.start_time
        
        self.process.start()
        # plot_thread = threading.Thread(target = speed_plot, daemon = True, args = (self.process, self.ports, self.stim_param, ))
        # plot_thread.start()
        self.timer.start(self._interval)
    
  
    def stop_process(self):
        self.process.stop()
        self.timer.stop()
    
    def load_values(self):       
        # readout
        if self.process.running:
            speeds = []
            for i in range(10): # down sample 10 times
                value = self.process.q.get()
                speeds.append(value[2])
                
            self.accum_stp = value[3]
            if not self.stim_param["command_source"]:
                self.opto_state = value[4]
                self.stim_state = value[5]
                self.trigger_state = value[6]
                self.reward_state = value[8]
                
            self.lick_state = value[7]
            # print('current time: ' + str(time.time() - self.start_time))
            
            # display states if the command is from ardnoid
            if not self.stim_param["command_source"]:
                self.trial_led.setChecked(self.trigger_state)
                self.stim_led.setChecked(self.stim_state)
                self.opto_led.setChecked(self.opto_state)
                self.reward_led.setChecked(self.reward_state)
            
            self.lick_led.setChecked(self.lick_state)
            
            # set stimulus
            if self.stim_param["command_source"]:
                self.drivers(**self.stim_param)
                if self.stim_param["reward"]:
                    if ((time.time() - self.stim_start_time <= self.stim_param["respond_window"]) 
                    and (self.stim_start_time != self.start_time) 
                    and (not self.respond)) :
                        if self.lick_state:
                            self.respond = True
                            if self.stim_state:
                                self.reward_state = 1
                                self.reward_led.setChecked(1)
                                reward_driver(self.stim_param["reward_pulse_width"], port = self.ports[0])
                            else:
                                self.time_out()
                    elif time.time() - self.stim_start_time > self.stim_param["respond_window"]:
                        self.respond = False
                        self.reward_state = 0
                        self.reward_led.setChecked(0)
                
            # Add x and y to lists
            self.x.append(time.time() - self.start_time)
            self.y.append(np.mean(speeds))
            
            # Limit x and y lists to 20 items
            self.x = self.x[-20:]
            self.y = self.y[-20:]
            self.curve.setData(self.x, self.y)
            # self.print_speed()

        else:
            pass

        
    def drivers(self, command_source,
                opto, intensity, opto_p, freq, dur, stim, stim_p, step_num, sps,
                trigger, trigger_type, turns_before, turns_during, reward, 
                reward_pulse_width, time_out_dur, respond_window):
        """
        drivers based on kwaargs, called in the while loop inside speed_plot stimulus part
    
        Parameters
        ----------
        **kwargs : dict
            opto (bool)
            stim (bool)
            trigger (bool)
            reward(bool)
            
            opto intensity
            (opto probability)
            (opto freq)
            (opto dur)
            
            (stim probability)
            (stim step_num)
            (stim sps)
            
            trigger_type (1 for level, 0 for edge)
            (turns_before)
            (turns_during)
            
            (reward_pulse_width) this is a in-built value, can't be defined 
            from gui'
    
        Returns
        -------
        None.
    
        """
        
        if self.trigger_command_state:
            if ((self.accum_stp >= self.last_stp + 20) and (not self.stim_flag)): 
                #animal runs for half turn before stimulation 
                self.stim_start_time = time.time()
                self.stim_flag = True
                if opto:
                    if np.random.uniform()<=opto_p:
                        self.opto_led.setChecked(1)
                        self.opto_state = 1
                        opto_driver(intensity, port = self.ports[0],freq = freq, dur = dur)
                if stim:
                    if np.random.uniform()<=stim_p:
                        self.stim_led.setChecked(1)
                        self.stim_state = 1
                        stepper_driver(port = self.ports[0], step_num = step_num, sps = sps, dur = dur)
            if self.accum_stp >= self.last_stp + 40 *turns_during:
                if ((time.time() - self.stim_start_time <= 1) 
                    and (self.stim_start_time != self.start_time)):
                    print("trial too short, wait till 1 s")
                    time.sleep(1 - (time.time() - self.stim_start_time))
                self.opto_led.setChecked(0)
                self.stim_led.setChecked(0)
                self.opto_state = 0
                self.stim_state = 0
                self.trigger_state = 0
                self.stim_flag = False
                self.last_stp = self.accum_stp
                self.trigger_command_state = 0
                self.trial_led.setChecked(0)
                if trigger_type:
                    trigger_level(0, port = self.ports[0])
                else:
                    trigger_pulse(port = self.ports[0])
        else:
            if self.accum_stp >= self.last_stp + 40*turns_before:
                self.last_stp = self.accum_stp
                self.trigger_state = 1
                self.trigger_command_state = 1
                self.trial_led.setChecked(1)
                if trigger_type:
                    trigger_level(1, port = self.ports[0])
                else:
                    trigger_pulse(port = self.ports[0])
               
    def manual_opto(self):
        intensity = self.stim_param['intensity']
        freq = self.stim_param['freq']
        dur = self.stim_param['dur']
        opto_driver(intensity, port = self.ports[0],freq = freq, dur = dur)
        
    def manual_stim(self):
        step_num = self.stim_param['step_num']
        sps = self.stim_param['sps']
        dur = self.stim_param['dur']
        stepper_driver(port = self.ports[0], step_num = step_num, sps = sps, dur = dur)
        
    def manual_camera(self):
        trigger_level(1, port = self.port[0], bit_num = 12, board_num = 0)
        
    def manual_camera_stop(self):
        trigger_level(0, port = self.port[0], bit_num = 12, board_num = 0)
            
    def time_out(self):
        print("time out for %.2f seconds" % self.stim_param["time_out_dur"])
        time.sleep(self.stim_param["time_out_dur"])
        
    def print_speed(self):
        print('current time: ' + str(self.x[-1]) + ' current speed: ' + str(self.y[-1]))        
        
    def changeStyle(self, styleName):
        QApplication.setStyle(QStyleFactory.create(styleName))
        self.changePalette()
        
    def changePalette(self):
        QApplication.setPalette(QApplication.style().standardPalette())

    # def advanceProgressBar(self):
    #     curVal = self.progressBar.value()
    #     maxVal = self.progressBar.maximum()
    #     self.progressBar.setValue(curVal + (maxVal - curVal) / 100)

    def createTopLeftGroupBox(self):
        self.topLeftGroupBox = QGroupBox("Opto_Param")
        intensity_Label = QLabel("Intensity")
        p_Label = QCheckBox("Probability")
        freq_Label = QCheckBox("Freq")
        dur_Label = QCheckBox("Dur")
        p_Label.setChecked(False)
        freq_Label.setChecked(False)
        dur_Label.setChecked(False)

        self.intensity_text  = QLineEdit("100")
        self.opto_p_text  = QLineEdit("0.5")
        self.freq_text  = QLineEdit("980")
        self.dur_text  = QLineEdit("20")
        
        self.intensity_text.textChanged[str].connect(self.set_intensity)
        p_Label.toggled.connect(self.set_opto_p)
        freq_Label.toggled.connect(self.set_freq)
        dur_Label.toggled.connect(self.set_dur)
        

        layout = QGridLayout()
        layout.addWidget(intensity_Label, 0, 0)
        layout.addWidget(self.intensity_text, 0, 1)
        layout.addWidget(p_Label, 1, 0)
        layout.addWidget(self.opto_p_text, 1, 1)
        layout.addWidget(freq_Label, 2, 0)
        layout.addWidget(self.freq_text, 2, 1)
        layout.addWidget(dur_Label, 3, 0)
        layout.addWidget(self.dur_text, 3, 1)
        # layout.addStretch(1)
        self.topLeftGroupBox.setLayout(layout)    

    def createTopRightGroupBox(self):
        # p = 0.5, step_num = 20, sps = 100
        self.topRightGroupBox = QGroupBox("Post_Stim_Param")
        p_Label = QCheckBox("Probability")
        step_num_Label = QCheckBox("Step num")
        sps_Label = QCheckBox("Step per second")
        p_Label.setChecked(False)
        step_num_Label.setChecked(False)
        sps_Label.setChecked(False)

        self.stim_p_text  = QLineEdit("0.5")
        self.step_num_text  = QLineEdit("50")
        self.sps_text  = QLineEdit("10")
        
        p_Label.toggled.connect(self.set_stim_p)
        step_num_Label.toggled.connect(self.set_step_num)
        sps_Label.toggled.connect(self.set_sps)
        

        layout = QGridLayout()
        layout.addWidget(p_Label, 0, 0)
        layout.addWidget(self.stim_p_text, 0, 1)
        layout.addWidget(step_num_Label, 1, 0)
        layout.addWidget(self.step_num_text, 1, 1)
        layout.addWidget(sps_Label, 2, 0)
        layout.addWidget(self.sps_text, 2, 1)
        # layout.addStretch(1)
        self.topRightGroupBox.setLayout(layout)  

        
    def createBottomLeftGroupBox(self):
        # trigger_type = 1, turns_before = 2, turns_during = 2
        self.bottomLeftGroupBox = QGroupBox("Trigger_param")
        trigger_type_Label = QCheckBox("Trigger Type, (1 for level, 0 for edge)")
        turns_before_Label = QCheckBox("Turns before trial starts")
        turns_during_Label = QCheckBox("Turns before trial stops")
        trigger_type_Label.setChecked(False)
        turns_before_Label.setChecked(False)
        turns_during_Label.setChecked(False)

        self.trigger_type_text  = QLineEdit("1")
        self.turns_before_text  = QLineEdit("2")
        self.turns_during_text  = QLineEdit("2")
        
        trigger_type_Label.toggled.connect(self.set_trigger_type)
        turns_before_Label.toggled.connect(self.set_turns_before)
        turns_during_Label.toggled.connect(self.set_turns_during)
        

        layout = QGridLayout()
        layout.addWidget(trigger_type_Label, 0, 0)
        layout.addWidget(self.trigger_type_text, 0, 1)
        layout.addWidget(turns_before_Label, 1, 0)
        layout.addWidget(self.turns_before_text, 1, 1)
        layout.addWidget(turns_during_Label, 2, 0)
        layout.addWidget(self.turns_during_text, 2, 1)
        # layout.addStretch(1)
        self.bottomLeftGroupBox.setLayout(layout) 
        
        
    # def createBottomLeftTabWidget(self):
    #     self.bottomLeftTabWidget = QTabWidget()
    #     self.bottomLeftTabWidget.setSizePolicy(QSizePolicy.Preferred,
    #             QSizePolicy.Ignored)

    #     tab1 = QWidget()
    #     tableWidget = QTableWidget(10, 10)

    #     tab1hbox = QHBoxLayout()
    #     tab1hbox.setContentsMargins(5, 5, 5, 5)
    #     tab1hbox.addWidget(tableWidget)
    #     tab1.setLayout(tab1hbox)

    #     tab2 = QWidget()
    #     textEdit = QTextEdit()

    #     textEdit.setPlainText("Twinkle, twinkle, little star,\n"
    #                           "How I wonder what you are.\n" 
    #                           "Up above the world so high,\n"
    #                           "Like a diamond in the sky.\n"
    #                           "Twinkle, twinkle, little star,\n" 
    #                           "How I wonder what you are!\n")

    #     tab2hbox = QHBoxLayout()
    #     tab2hbox.setContentsMargins(5, 5, 5, 5)
    #     tab2hbox.addWidget(textEdit)
    #     tab2.setLayout(tab2hbox)

    #     self.bottomLeftTabWidget.addTab(tab1, "&Table")
    #     self.bottomLeftTabWidget.addTab(tab2, "Text &Edit")

    def createBottomRightGroupBox(self):
        self.bottomRightGroupBox = QGroupBox("Start")

        self.loadPushButton = QPushButton("&Load")
        self.loadPushButton.setDefault(True)
        self.loadPushButton.clicked.connect(self.board_setup)

        self.startPushButton = QPushButton("Start")
        self.startPushButton.setDefault(True)
        self.startPushButton.clicked.connect(self.start_process)

        self.stopPushButton = QPushButton("&Stop")
        self.stopPushButton.setDefault(True)
        self.stopPushButton.clicked.connect(self.stop_process)
        
        self.water_rewardPushButton = QPushButton("test_water_reward")
        self.water_rewardPushButton.setDefault(False)
        
        reward_pulse_width_Label = QCheckBox("Reward pulse width")
        reward_pulse_width_Label.setChecked(False)
        self.reward_pulse_width_text  = QLineEdit("0.1")
        
        reward_pulse_width_Label.toggled.connect(self.set_reward_pulse_width)
        try:
            self.water_rewardPushButton.clicked.connect(reward_driver(self.stim_param["reward_pulse_width"], port = self.ports[0]))
        except TypeError:
            print("Load the DAQ first")

        layout = QGridLayout()
        layout.addWidget(self.loadPushButton, 0, 0, 1, 2)
        layout.addWidget(self.startPushButton, 1, 0)        
        layout.addWidget(self.stopPushButton, 1, 1)
        layout.addWidget(reward_pulse_width_Label, 2, 0)
        layout.addWidget(self.reward_pulse_width_text, 2, 1)
        layout.addWidget(self.water_rewardPushButton, 2, 2)
        self.bottomRightGroupBox.setLayout(layout)
        
    def createBottomLeftGroupBox2(self):
        self.bottomLeftGroupBox2 = QGroupBox("Stim State")

        self.trial_led = LedIndicator()
        self.trial_led.setDisabled(True)
        trial_led_label = QLabel("trial state")
        
        self.stim_led = LedIndicator()
        self.stim_led.setDisabled(True)
        stim_led_label = QLabel("stim state")
        
        self.opto_led = LedIndicator()
        self.opto_led.setDisabled(True)
        opto_led_label = QLabel("opto state")

        layout = QGridLayout()
        layout.addWidget(trial_led_label, 0, 0)
        layout.addWidget(stim_led_label, 1, 0)
        layout.addWidget(opto_led_label, 2, 0)
        layout.addWidget(self.trial_led, 0, 1)
        layout.addWidget(self.stim_led, 1, 1)
        layout.addWidget(self.opto_led, 2, 1)
        self.bottomLeftGroupBox2.setLayout(layout)
        
    def createBottomRightGroupBox2(self):
        self.bottomRightGroupBox2 = QGroupBox("lick and reward State")

        self.lick_led = LedIndicator()
        self.lick_led.setDisabled(True)
        lick_led_label = QLabel("lick state")
        
        self.reward_led = LedIndicator()
        self.reward_led.setDisabled(True)
        reward_led_label = QLabel("reward state")

        layout = QGridLayout()
        layout.addWidget(lick_led_label, 0, 0)
        layout.addWidget(reward_led_label, 1, 0)
        layout.addWidget(self.lick_led, 0, 1)
        layout.addWidget(self.reward_led, 1, 1)
        self.bottomRightGroupBox2.setLayout(layout)

    # def createProgressBar(self):
    #     self.progressBar = QProgressBar()
    #     self.progressBar.setRange(0, 10000)
    #     self.progressBar.setValue(0)

    #     timer = QTimer(self)
    #     timer.timeout.connect(self.advanceProgressBar)
    #     timer.start(1000)
        
    def createBottomPanel(self):
        self.speedplot = pg.PlotWidget()
        self.speedplot.enableAutoRange()
        self.speedplot.setBackground('w')
        
        self.speedplot.showGrid(x=True, y=True)
        self.speedplot.setLabel('left', 'speed', 'clicks per 1ms')
        self.speedplot.setLabel('bottom', 'time', 's')
        self.curve = self.speedplot.plot(self.x, self.y)
        
    def createBottomPanel2(self):
        self.bottomGroupBox2 = QGroupBox("Manual control")

        self.manualOptoPushButton = QPushButton("opto")
        self.manualOptoPushButton.setDefault(False)
        self.manualOptoPushButton.clicked.connect(self.manual_opto)

        self.manualStimPushButton = QPushButton("Stim")
        self.manualStimPushButton.setDefault(False)
        self.manualStimPushButton.clicked.connect(self.manual_stim)

        self.manualCameraPushButton = QPushButton("Camera")
        self.manualCameraPushButton.setDefault(False)
        self.manualCameraPushButton.clicked.connect(self.manual_camera)
        
        self.manualCameraStopPushButton = QPushButton("Camera stop")
        self.manualCameraStopPushButton.setDefault(False)
        self.manualCameraStopPushButton.clicked.connect(self.manual_camera_stop)
        
        layout = QGridLayout()
        layout.addWidget(self.manualOptoPushButton, 0, 0)
        layout.addWidget(self.manualStimPushButton, 0, 1)
        layout.addWidget(self.manualCameraPushButton, 0, 2)
        layout.addWidget(self.manualCameraStopPushButton, 0, 3)
        self.bottomGroupBox2.setLayout(layout)
        
        

if __name__ == '__main__':

    app = QApplication(sys.argv)
    gallery = Behavioral_GUI()
    gallery.show()
    sys.exit(app.exec_()) 

