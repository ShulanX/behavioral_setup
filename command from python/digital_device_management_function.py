# -*- coding: utf-8 -*-
"""
Created on Sun Nov 24 19:27:59 2019

@author: dalan

The USB-1024LS device has 24 channels distributed in 4 ports
port 0: FIRSTPORTA    0 to 7 bit
port 1: FIRSTPORTB    8 to 15 bit
port 2: FIRSTPORTCL    16 to 19 bit
port 3: FIRSTPORTCH    20 to 23 bit

"""

from __future__ import absolute_import, division, print_function
from mcculw import ul
from mcculw.enums import DigitalIODirection, DigitalPortType
from mcculw.enums import ScanOptions, BoardInfo
from digital import DigitalProps
import util
from mcculw.ul import ULError

import ctypes

def load_digital_in_port(board_num = 0):
    """
    load the first digital input ports

    Parameters
    ----------
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    port: all the digital input ports

    """
    use_device_detection = True
    
    if use_device_detection:
        ul.ignore_instacal()
        if not util.config_first_detected_device(board_num):
            print("Could not find device.")
            return

    digital_props = DigitalProps(board_num)

    # Find the first port that supports input, defaulting to None
    # if one is not found.
    port = next(
        (port for port in digital_props.port_info
         if port.supports_input), None)
    if port == None:
        util.print_unsupported_example(board_num)
        return

    try:
        # If the port is configurable, configure it for input.
        if port.is_port_configurable:
            ul.d_config_port(board_num, port.type, DigitalIODirection.IN)

        return port
    
    except ULError as e:
        util.print_ul_error(e)
    # finally:
    #     if use_device_detection:
    #         ul.release_daq_device(board_num)
        

def load_digital_out_port(board_num = 0):
    """
    load the first digital output ports

    Parameters
    ----------
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    port: the first digital output ports

    """
    use_device_detection = True
    
    if use_device_detection:
        ul.ignore_instacal()
        if not util.config_first_detected_device(board_num):
            print("Could not find device.")
            return

    digital_props = DigitalProps(board_num)

    # Find the first port that supports input, defaulting to None
    # if one is not found.
    port = next(
        (port for port in digital_props.port_info
         if port.supports_output), None)
    if port == None:
        util.print_unsupported_example(board_num)
        return

    try:
        # If the port is configurable, configure it for input.
        if port.is_port_configurable:
            ul.d_config_port(board_num, port.type, DigitalIODirection.OUT)

        return port
    
    except ULError as e:
        util.print_ul_error(e)
    # finally:
    #     if use_device_detection:
    #         ul.release_daq_device(board_num)

def load_all_digital_ports(board_num = 0):
    """
    load all digital ports on the board

    Parameters
    ----------
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    None.

    """
    use_device_detection = True
    
    if use_device_detection:
        ul.ignore_instacal()
        if not util.config_first_detected_device(board_num):
            print("Could not find device.")
            return

    digital_props = DigitalProps(board_num)
    return digital_props.port_info
    
    
def set_digital_in_channels(port, board_num = 0):
    """
    set the port to be digital input if configurable

    Parameters
    ----------
    port : DigitalPortType
        returned from load_digital_in_port
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    None.

    """
    if port.is_port_configurable:
        ul.d_config_port(board_num, port.type, DigitalIODirection.IN)
  
    
def set_digital_out_channels(port, board_num = 0): 
    """
    set the port to be digital output if configurable

    Parameters
    ----------
    port : DigitalPortType
        returned from load_digital_in_port
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    None.

    """
    if port.is_port_configurable:
        ul.d_config_port(board_num, port.type, DigitalIODirection.OUT)
  
    
def digital_bit_in(bit_num, port, board_num = 0, verbool = False):
    """
    read the bit value at bit_num from a port

    Parameters
    ----------
    bit_num : TYPE
        DESCRIPTION.
    port : DigitalPortType
        DESCRIPTION.
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.
    verbool : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    bit_value : bool
        DESCRIPTION.

    """
    try:
        bit_value = ul.d_bit_in(board_num, port.type, bit_num)

        # Display the bit value
        if verbool:
            print(port.type.name + " Bit " + str(bit_num) + " Value: " + str(bit_value))
        return bit_value
    except ULError as e:
        util.print_ul_error(e)


def digital_bit_out(bit_value, bit_num, port, board_num = 0, verbool = False):
    """
    write the bit value at bit_num from a port

    Parameters
    ----------
    bit_num : TYPE
        DESCRIPTION.
    port : DigitalPortType
        DESCRIPTION.
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.
    verbool : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    bit_value : bool
        DESCRIPTION.

    """
    try:
        ul.d_bit_out(board_num, port.type, bit_num, bit_value)

        # Display the bit value
        if verbool:
            print(port.type.name + "Bit " + str(bit_num) + " Value: " + str(bit_value))
    except ULError as e:
        util.print_ul_error(e)
        
        
def digital_port_in(port, board_num = 0, verbool = False):
    """
    read the port value    
    
    Parameters
    ----------
    port : DigitalPortType
        DESCRIPTION.
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.
    verbool : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    port_value : int
        0 to 255 or 0 to 15 depend on the port

    """
    try:
        port_value = ul.d_in(board_num, port.type)

        # Display the bit value
        if verbool:
            print(port.type.name + " Value: " + str(port_value))
        return port_value
    except ULError as e:
        util.print_ul_error(e)
        
        
def digital_in_scan(port, rate = 1000, counts = 1000, board_num = 0, verbool = False):
    """
    scan the amount of counts input from the port, note that a counter signal
    may be required for the scan

    Parameters
    ----------
    port : TYPE
        DESCRIPTION.
    rate : int, Hz
        rate of scan. The default is 1000.
    counts : int
        The total count of one scan. The default is 1000.
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.
    verbool : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    data_out : TYPE
        the array of scanned data

    """
    
    scan_options = ScanOptions.FOREGROUND | ScanOptions.SCALEDATA

    memhandle = ul.scaled_win_buf_alloc(counts)
    # Convert the memhandle to a ctypes array.
    # Use the memhandle_as_ctypes_array_scaled method for scaled
    # buffers.
    ctypes_array = util.memhandle_as_ctypes_array_scaled(memhandle)
    # Note: the ctypes array will no longer be valid after win_buf_free is
    # called.
    # A copy of the buffer can be created using win_buf_to_array or
    # win_buf_to_array_32 before the memory is freed. The copy can be used
    # at any time.
    

    # Check if the buffer was successfully allocated
    if not memhandle:
        print("Failed to allocate memory.")
        return

    try:

        # Start the scan
        ul.d_in_scan(board_num, port.type, counts, rate, memhandle, scan_options)
        if verbool:
            print("Scan completed successfully. Data:")
    
            # Print the data
            data_index = 0
            for index in range(counts):
                # Print this row
                print(str(index) + ' value: ' + str(ctypes_array[data_index]))
        
        data_out = list(ctypes_array)        
        return data_out
    
    except ULError as e:
        util.print_ul_error(e)
    finally:
        # Free the buffer in a finally block to prevent errors from causing
        # a memory leak.
        ul.win_buf_free(memhandle)

        # if use_device_detection:
        #     ul.release_daq_device(board_num)


def digital_in_array(lower_port, higher_port, data_array = None, board_num = 0, verbool = False):
    """
    synchronous reading out from a series of ports. Note that data_array may 
    need to be a pointer

    Parameters
    ----------
    lower_port : DigitalPortType
        The lowest port in the series
    higher_port : DigitalPortType
        The highest port in the series
    data_array : TYPE, optional
        May need to be a pointer, prevous allocation of data array to avoid memmory problem. 
        The default is None.
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.
    verbool : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    None.

    """
    try:
        ul.d_in_array(board_num, lower_port, higher_port, data_array)

        # Display the array value
        if verbool:
            for port_num in range(len(data_array)):
                print(str(port_num) + " Value: " + data_array[port_num])
    except ULError as e:
        util.print_ul_error(e)