whisker related behavioral set up on the recording rig, will include the following features:

1. decoding the rotary encoder
2. start/stop trials (which will start the high speed camera recording) based on running behavior
3. give optogenetic stimulation (with fiber LED) randomly at a defined portion of trials
4. give whisker stimulation (with a post bring with the stepper motor) randomly at a defined portion of trials
5. detect licking and give reward (for correct detection) or time out period (for false alarm)
6. manually giving optogenetic stim or whisker stim, manurally start or stop camera recording

hardware required (see the circuit connection in the circuit file):
1. digital USB DAQ
2. 1 arduino uno for rotary decoding

Run behavioral_setup_gui.py from either spider or cmd (by cd to the directory and run the command "python behavioral_setup_gui.py"), in the gui load the USB DAQ before start the trials, the parameters can also be specified in the gui.

python version 3.7.3 is used
