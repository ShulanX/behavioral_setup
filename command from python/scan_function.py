# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 10:59:32 2019

@author: dalan

function to scan the input/output ports and read out the values

class:
    ScanThreading: 

hardware connection:
    
input:
port 0 bit 0: Wheelstp
port 0 bit 1: Wheeldir
port 0 bit 2: opto
port 0 bit 3: stim
port 0 bit 4: trigger
port 0 bit 5: lick
port 0 bit 6: reward
output:
port 1 bit 0 (bit 8): optogenetic driver
port 1 bit 1-2 (bit 9-10): stepper motor driver (DIR and PUL)
port 1 bit 3 (bit 11): opto enable signal 
port 1 bit 4 (bit 12): stim enable signal
port 1 bit 5 (bit 13): trigger
port 1 bit 6 (bit 15): water reward
"""

import time
import datetime
import threading
import numpy as np
from digital_device_management_function import load_digital_in_port, digital_port_in, digital_bit_in, digital_bit_out


class ScanTreading(object):
    def __init__(self, q, port = None, fs = 100e3, board_num = 0):
        """
        

        Parameters
        ----------
        q : queue.Queue
            A FIFO queue for the read out, each list in the queue is consist of:
            [wheelstp, wheeldir, speed, accum_stp, opto_state, stim_state, trigger_state]
        port : TYPE, optional
            DESCRIPTION. The default is None.
        fs : TYPE, optional
            DESCRIPTION. The default is 100e3.
        board_num : TYPE, optional
            DESCRIPTION. The default is 0.

        Returns
        -------
        None.

        """
        self.port = port
        self.fs = fs
        self.board_num = board_num
        self.running = False
        self.q = q
        self.thread = threading.Thread(target = self.run, daemon = True)
        
    def load(self):
        if self.port is None:
            self.port = load_digital_in_port(board_num = self.board_num)
            
    def start(self):
        self.running = True
        self.thread.start()
        
        
    def stop(self):
        self.running = False
        self.thread.join()
        # self.q.join()
        
        
    def run(self):
        """
        while self.running is true, scan the port and return the values

        Returns
        -------
        None.

        """
        speed_array = np.zeros(int(self.fs/1000), dtype = np.uint8)
        accum_stp = int(0)
        while self.running:
            v = digital_port_in(self.port, self.board_num, verbool = False)
            value = np.unpackbits(np.uint8(v))
            speed_array = np.append(value[7]&value[6], speed_array[0:-1])
            speed = np.sum(speed_array)
            wheelstp = value[7]
            wheeldir = value[6]
            opto_state = value[5]
            stim_state = value[4]
            trigger_state = value[3]
            lick_state = value[2]
            reward_state = value[1]
            accum_stp = accum_stp + int(value[7]&value[6])
            self.q.put([wheelstp, wheeldir, speed, accum_stp, opto_state, stim_state, trigger_state, lick_state, reward_state])
            time.sleep(1/self.fs)
            
def opto_driver(intensity, port, bit_num = [11,8], board_num = 0, freq = 980, dur = 1):
    """
    drive the LED in a PWM square wave manner, the frequency of square wave
    default to be 980 Hz, duration is default to be 1s

    Parameters
    ----------
    intensity : int (0-255)
        the duration of square wave
    bit_num : int
        The output bit number
    port : TYPE
        DESCRIPTION.
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    None.

    """
    driver_param = {
        'intensity': intensity,
        'port': port,
        'bit_num': bit_num,
        'board_num': board_num, 
        'freq': freq,
        'dur': dur,
        }
    opto_driver_thread = threading.Thread(target = opto_driver_fun, 
                                          daemon = True, kwargs = driver_param)
    opto_driver_thread.start()
    
def reward_driver(reward_pulse_width, port, bit_num = 14, board_num = 0):
    """
    drive the reward signal, the pulse width is set with reward_pulse_width

    Returns
    -------
    None.

    """
    driver_param = {
        'reward_pulse_width': reward_pulse_width,
        'port': port,
        'bit_num': bit_num,
        'board_num': board_num, 
        }
    reward_driver_thread = threading.Thread(target = reward_driver_fun, 
                                          daemon = True, kwargs = driver_param)
    reward_driver_thread.start()

def stepper_driver(port, bit_num = [12, 9, 10], board_num = 0, step_num = 20, sps = 100, dur = 1):
    """
    hardware connection: 1-2 bit will connect to DIR and PUL expectively
    the stepper motor has resolution of 1.8 degree

    Parameters
    ----------
    port : TYPE
        DESCRIPTION.
    bit_num : TYPE, optional
        DESCRIPTION. The default is 1.
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.
    step_num : numeric, optional
        number of total steps. The default is 20 (36 degree).
    sps : numeric, optional
        step per second. The default is 100.
    dur : 
        duration of the post presence in the whisker field
        
    Returns
    -------
    None.

    """
    driver_param = {
        'port': port,
        'bit_num': bit_num,
        'board_num': board_num, 
        'step_num': step_num,
        'sps': sps,
        'dur': dur
        }
    stepper_driver_thread = threading.Thread(target = stepper_driver_fun, 
                                          daemon = True, kwargs = driver_param)
    stepper_driver_thread.start()
    
 
def trigger_level(is_high, port, bit_num = 13, board_num = 0):
    """
    send trigger signal as level (high level for trigger on, low level for 
    trigger off)

    Parameters
    ----------
    is_high : bool
        if the trigger will be put to high or not
    port : DigitalPortType
        DESCRIPTION.
    bit_num : TYPE, optional
        DESCRIPTION. The default is 5.
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    None.

    """
    digital_bit_out(is_high, bit_num, port, board_num)
    
def trigger_pulse(port, bit_num = 13, board_num = 0, dur = 1e-3):
    """
    send trigger signal as pulse 

    Parameters
    ----------
    port : TYPE
        DESCRIPTION.
    bit_num : TYPE, optional
        DESCRIPTION. The default is 5.
    board_num : TYPE, optional
        DESCRIPTION. The default is 0.
    dur : TYPE, optional
        durtion of the pulse in second. The default is 1e-3 (1ms).

    Returns
    -------
    None.

    """
    pulse_param = {
        'port': port,
        'bit_num': bit_num,
        'board_num': board_num, 
        'dur': dur,
        }
    pulse_thread = threading.Thread(target = pulse_fun, 
                                          daemon = True, kwargs = pulse_param)
    pulse_thread.start()
    pulse_thread.join()
    
def opto_driver_fun(intensity, port, bit_num, board_num, freq, dur):
    print('calling optical stimulation')
    digital_bit_out(1, bit_num[0], port, board_num)
    periods = int(dur*freq)
    i = 0
    high_time = 1/freq*intensity/256
    low_time = 1/freq - high_time
    while i <= periods:
        digital_bit_out(0, bit_num[1], port, board_num)
        time.sleep(low_time)
        digital_bit_out(1, bit_num[1], port, board_num)
        time.sleep(high_time)
        i = i+1
    digital_bit_out(0, bit_num[0], port, board_num)
        
def reward_driver_fun(reward_pulse_width, port, bit_num = 15, board_num = 0):
    print('delivering reward')
    print(bit_num)
    digital_bit_out(1, bit_num, port, board_num)
    time.sleep(reward_pulse_width)
    digital_bit_out(0, bit_num, port, board_num)
    
def stepper_driver_fun(port, bit_num, board_num, step_num, sps, dur):
    print('calling stepper motor')
    half_p = int(1/sps/2);
    i = 0
    # halfstep_seq = [
    #     [1,0,0,0],
    #     [1,1,0,0],
    #     [0,1,0,0],
    #     [0,1,1,0],
    #     [0,0,1,0],
    #     [0,0,1,1],
    #     [0,0,0,1],
    #     [1,0,0,1]
    # ]
    digital_bit_out(1, bit_num[1], port, board_num)
    while i <= step_num:
        # for halfstep in range(8):
#        for bit in bit_num:
        digital_bit_out(1, bit_num[2], port, board_num)
        time.sleep(half_p)
        digital_bit_out(0, bit_num[2], port, board_num)
        time.sleep(half_p)
        i = i+1
        
    digital_bit_out(1, bit_num[0], port, board_num)
        
    time.sleep(int(dur - step_num/sps*2))
    digital_bit_out(0, bit_num[0], port, board_num)
    
    # halfstep_seq = [
    #     [1,0,0,1],
    #     [0,0,0,1],
    #     [0,0,1,1],
    #     [0,0,1,0],
    #     [0,1,1,0],
    #     [0,1,0,0],
    #     [1,1,0,0],
    #     [1,0,0,0]   
    # ]
    digital_bit_out(0, bit_num[1], port, board_num)
    i = 0;
    while i <= step_num:
        # for halfstep in range(8):
#        for bit in bit_num:
        digital_bit_out(1, bit_num[2], port, board_num)
        time.sleep(half_p)
        digital_bit_out(0, bit_num[2], port, board_num)
        time.sleep(half_p)
        i = i+1
    print("end calling stepper motor")
  
      
def pulse_fun(port, bit_num, board_num, dur):
    digital_bit_out(1, bit_num, port, board_num)
    time.sleep(dur)
    digital_bit_out(0, bit_num, port, board_num)
    
        
def test_thread():
    test = threading.Thread(target = test_fun)
    test_2 = threading.Thread(target = test_fun2)
    start = time.time()
    test.start()
    test_2.start()
    end = time.time()
    print(end-start)
    
    
def test_fun():
    time.sleep(1)
    print('after 1s')

def test_fun2():
    time.sleep(2)
    print('after 2s')
